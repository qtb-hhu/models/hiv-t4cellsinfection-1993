# HIV-t4cellsinfection-1993

modelbase v1.0 implementation of the original model published in 1993 in Mathematical Biosciences 114:81-125(1993) "Dynamics of HIV Infection of CD4+ T cells" by ALAN S. PERELSON, DENISE E. KIRSCHNER and ROB DE BOER
